package missions.init;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import missions.dto.User;
import missions.services.UserService;
import missions.util.PasswordUtil;
import missions.util.ProjectConfig;


@WebListener
public class Initializer implements ServletContextListener{

	private Logger logger = Logger.getLogger(Initializer.class.getName());

	@Autowired
	UserService service;

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
//		ServletContextListener.super.contextDestroyed(sce);
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		logger.info("Initializing the context");

		ProjectConfig config = new ProjectConfig();

		String imagesPath = config.getImagesPath();

		if(!new File(imagesPath).exists())
			new File(imagesPath).mkdirs();

		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

		try {
			User superUser = new User();
			superUser.setUserName("superadmin");
			String password = PasswordUtil.getSaltedHash("123456");
			superUser.setPassword(password);
			superUser.setUserName("superadmin");
			superUser.setDefaultPassword(true);
			superUser.setEnable(true);
			superUser.setUserTitle("SUPER");
			service.createUser(superUser, null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
