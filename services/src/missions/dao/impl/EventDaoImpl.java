package missions.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import missions.dao.EventDao;
import missions.dto.Attachments;
import missions.dto.Event;
import missions.dto.EventJoinQuestionnaire;
import missions.dto.User;

@Repository
public class EventDaoImpl implements EventDao{

	private Logger logger = Logger.getLogger(EventDaoImpl.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Event createEvent(Event event) {
		logger.info("Creating event with Id "+event.getEventId());
		sessionFactory.getCurrentSession().save(event);
		return event;
	}

	@Override
	public boolean save(EventJoinQuestionnaire answer) {
		logger.info("Saving answer");
		sessionFactory.getCurrentSession().save(answer);
		return true;
	}

	@Override
	public boolean saveAttachment(Attachments attachment) {
		logger.info("Saving attachment");
		sessionFactory.getCurrentSession().save(attachment);
		return true;
	}

	@Override
	public List<Event> getReportsofaUser(User user, String reportType) {

		logger.info("Get reports of a user with id "+user.getUserId());
		
		CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Event> eventQuery = cb.createQuery(Event.class);
		Root<Event> eventRoot = eventQuery.from(Event.class);
		eventQuery.where(cb.equal(eventRoot.get("user"), user));

		return sessionFactory.getCurrentSession().createQuery(eventQuery).getResultList();
	}

	@Override
	public List<Event> getAllEventsWithNulls() {
		CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Event> eventQuery = cb.createQuery(Event.class);
		Root<Event> eventRoot = eventQuery.from(Event.class);
		eventQuery.where(cb.equal(eventRoot.get("reportType"), "EVENT"));
		return sessionFactory.getCurrentSession().createQuery(eventQuery).getResultList();
	}

	@Override
	public void saveOrUpdate(Event e) {
		sessionFactory.getCurrentSession().saveOrUpdate(e);
	}

	@Override
	public void deleteEvents(Event event) {
		sessionFactory.getCurrentSession().delete(event);
	}

}
