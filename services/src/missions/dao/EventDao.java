package missions.dao;

import java.util.List;

import missions.dto.Attachments;
import missions.dto.Event;
import missions.dto.EventJoinQuestionnaire;
import missions.dto.User;

public interface EventDao {
	public Event createEvent(Event event);

	public boolean save(EventJoinQuestionnaire answer);

	public boolean saveAttachment(Attachments attachment);

	public List<Event> getReportsofaUser(User user, String reportType);

	public List<Event> getAllEventsWithNulls();

	public void saveOrUpdate(Event e);
	
	public void deleteEvents(Event user);

}
