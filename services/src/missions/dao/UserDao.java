package missions.dao;

import java.util.List;

import missions.dto.User;
import missions.dto.UserAddress;

public interface UserDao {
	public User getUserByName(String userName);

	public User registerUser(User user);

	public boolean updateUser(User user);

	public User getUserById(String userId);
	
	public User getUserByMobile(String mobileNo);

	public boolean save(UserAddress address);

	public List<User> searchUser(String firstName, String lastName, String phNum, String city, String userid);
}
