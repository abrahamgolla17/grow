package missions.services.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import missions.dto.Attachments;
import missions.dto.Event;
import missions.dto.EventJoinQuestionnaire;
import missions.dto.Questionnaire;
import missions.dto.User;
import missions.dto.UserAddress;

public class Convert2Transient {

	public Questionnaire convert2Transient(Questionnaire question) {
		if(question != null){
			Questionnaire persistedQuestion = new Questionnaire();
			persistedQuestion.setQuestion(question.getQuestion());
			persistedQuestion.setParent(convert2Transient(question.getParent()));
			persistedQuestion.setQuestionId(question.getQuestionId());
			persistedQuestion.setQuestionSection(question.getQuestionSection());
			persistedQuestion.setQuestionType(question.getQuestionType());
			persistedQuestion.setSerialNo(question.getSerialNo());
			persistedQuestion.setReportType(question.getReportType());
			return persistedQuestion;
		}
		return null;
	}
	
	public Questionnaire convert2TransientNull(Questionnaire question) {
		if(question != null){
			Questionnaire persistedQuestion = new Questionnaire();
			persistedQuestion.setQuestion(question.getQuestion());
			return persistedQuestion;
		}
		return null;
	}

	public User convert2TransientNull(User user) {
		if(user != null){
			User tUser = new User();
			tUser.setAddresses(null);
			tUser.setChurches(null);
			tUser.setEmailId(user.getEmailId());
			tUser.setMobileNumber(user.getMobileNumber());
			tUser.setUserId(user.getUserId());
			tUser.setUserImageUrl(user.getUserImageUrl());
			tUser.setUserName(user.getUserName());
			tUser.setUserTitle(user.getUserTitle());
			tUser.setFirstName(user.getFirstName());
			tUser.setLastName(user.getLastName());
			tUser.setDefaultPassword(user.isDefaultPassword());
			tUser.setGender(user.getGender());
			tUser.setUserImageUrl(user.getUserImageUrl());
			tUser.setTransactionPassword(user.getTransactionPassword());

			Set<UserAddress> addresses = new HashSet<>();
			for(UserAddress address : user.getAddresses())
				addresses.add(convert2Transient(address));

			tUser.setAddresses(addresses);
			return tUser;
		}
		return null;
	}

	private UserAddress convert2Transient(UserAddress address) {
		if(address != null){
			UserAddress uAddress = new UserAddress();
			uAddress.setAddressId(address.getAddressId());
			uAddress.setAddressLine1(address.getAddressLine1());
			uAddress.setAddressLine1(address.getAddressLine1());
			uAddress.setAddressLine2(address.getAddressLine2());
			uAddress.setAddressType(address.getAddressType());
			uAddress.setCity(address.getCity());
			uAddress.setState(address.getState());
			uAddress.setCountry(address.getCountry());
			uAddress.setLatitude(address.getLatitude());
			uAddress.setLongitude(address.getLongitude());
			uAddress.setPinCode(address.getPinCode());
			uAddress.setUser(null);
			return uAddress;
		}
		return null;
	}

	public Event convert2Transient(Event e) {

		if(e != null){
			Event event = new Event();
			event.setAreaName(e.getAreaName());
			event.setEventDate(e.getEventDate());
			event.setEventId(e.getEventId());
			event.setLanguage(e.getLanguage());
			event.setLatitude(e.getLatitude());
			event.setLongitude(e.getLongitude());
			event.setPincode(e.getPincode());
			event.setReportType(e.getReportType());
			event.setState(e.getState());

			List<Attachments> attachments = new ArrayList<>();
			for(Attachments a : e.getAttachments())
				attachments.add(convert2transient(a));
			event.setAttachments(attachments);

			List<EventJoinQuestionnaire> answers = new ArrayList<>();
			for(EventJoinQuestionnaire ejq : e.getEventsQuestionAnswers())
				answers.add(convert2Transient(ejq));
			event.setEventsQuestionAnswers(answers);

			event.setUser(null);

			return event;

		}

		return null;
	}

	public Attachments convert2transient(Attachments attch){
		if(attch != null){
			Attachments attachment = new Attachments();
			attachment.setAttachmentId(attch.getAttachmentId());
			attachment.setAttachmentThumnailUrl(attch.getAttachmentThumnailUrl());
			attachment.setAttachmentUrl(attch.getAttachmentUrl());
			attachment.setEvent(null);
			attachment.setCreatedDateTime(attch.getCreatedDateTime());
			attachment.setUpdatedDateTime(attch.getUpdatedDateTime());
			return attachment;
		}
		return null;
	}

	public EventJoinQuestionnaire convert2Transient(EventJoinQuestionnaire ejq){
		if(ejq != null){
			EventJoinQuestionnaire eventQuestionnaire = new EventJoinQuestionnaire();
			eventQuestionnaire.setAnswer(ejq.getAnswer());
			eventQuestionnaire.setQuestion(convert2TransientNull(ejq.getQuestion()));
			return eventQuestionnaire;
		}
		return null;
	}

}
