package missions.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import missions.dao.CommonDao;
import missions.dto.Event;
import missions.dto.EventJoinQuestionnaire;
import missions.dto.Questionnaire;
import missions.dto.User;
import missions.services.CommonServices;
import missions.services.util.Convert2Transient;

@Service
@Transactional
public class CommonServicesImpl implements CommonServices{

	private Logger logger = Logger.getLogger(CommonServicesImpl.class.getName());

	private static final String GROW_TRAINED = "If Yes, How many?";
	private static final String VIDEOS_SHOWN = "Number of Videos";
	private static final String PEOPLE_ACCEPTED = "No of People Accepted Christ";
	private static final String PEOPLE_BAPTIZED = "No of People Baptized";

	@Autowired
	CommonDao commondao;

	Convert2Transient convert;

	CommonServicesImpl(){
		convert = new Convert2Transient();
	}

	@Override
	public boolean createQuestionaire(Questionnaire questionnaire) {
		logger.info("Creating question");
		questionnaire.setQuestionId(createId("QUESTION"));
		Questionnaire question = commondao.saveQuestion(questionnaire);
		if(question != null)
			return true;
		return false;
	}

	@Override
	public List<Questionnaire> getAllQuestions() {
		logger.info("Get Questions");
		List<Questionnaire> allQuestions = new ArrayList<>();
		List<Questionnaire> questions = commondao.getQuestions();
		if(questions != null)
			for(Questionnaire question : questions)
				allQuestions.add(convert.convert2Transient(question));

		return allQuestions;
	}

	@Override
	public Map<String, Integer> getStatistics(long from, long till) {

		Map<String, Integer> statistics = new HashMap<>();
		long fromDate = from;
		long tillDate = till;
		int noOfPastorsReported = 0;
		int noOfVideosShown = 0;
		int noOfPeopleAcceptedChrist = 0;
		int noOfBaptisms = 0;
		int noOfEventsReported = 0;
		int noOfMonthlyReports = 0;

		Set<User> unique = new HashSet<>();

/*		if(fromDate == 0 || tillDate == 0){
			tillDate = new Date().getTime();
			Calendar c=new GregorianCalendar();
			c.add(Calendar.DATE, -30);
			Date d=c.getTime();
			fromDate = d.getTime();
		}*/
		
		List<Event> events = commondao.getStatsInAMonth(fromDate, tillDate);
		int growTrained = 0;
		for(Event e: events){
			unique.add(e.getUser());
			List<EventJoinQuestionnaire> ejqs = e.getEventsQuestionAnswers();
			if(e.getReportType().equalsIgnoreCase("MEDIA")){
				noOfMonthlyReports++;
				for(EventJoinQuestionnaire ejq : ejqs){
					Questionnaire q = ejq.getQuestion();
					if(q.getQuestion().equals(GROW_TRAINED)){
						growTrained += Integer.parseInt(ejq.getAnswer());
					}
				}
			}else if(e.getReportType().equalsIgnoreCase("EVENT")){
				noOfEventsReported++;
				for(EventJoinQuestionnaire ejq : ejqs){
					Questionnaire q = ejq.getQuestion();
					if(q.getQuestion().equals(VIDEOS_SHOWN)){
						noOfVideosShown += Integer.parseInt(ejq.getAnswer());
					}else if(q.getQuestion().equals(PEOPLE_ACCEPTED)){
						noOfPeopleAcceptedChrist+=Integer.parseInt(ejq.getAnswer());
					}else if(q.getQuestion().equals(PEOPLE_BAPTIZED)){
						noOfBaptisms+=Integer.parseInt(ejq.getAnswer());
					}
				}
			}
		}
		noOfPastorsReported = unique.size();

		statistics.put("NO_OF_PASTORS_REPORTED", noOfPastorsReported);
		statistics.put("NO_OF_GROW_TRAINED", growTrained);
		statistics.put("NO_OF_EVENT_REPORTS", noOfEventsReported);
		statistics.put("NO_OF_MONTHLY_REPORTS", noOfMonthlyReports);
		statistics.put("NO_OF_VIDEOS_SHOWN", noOfVideosShown);
		statistics.put("NO_OF_PEOPLE_ACCEPTED", noOfPeopleAcceptedChrist);
		statistics.put("NO_OF_PEOPLE_BAPTIZED", noOfBaptisms);

		return statistics;
	}

	private String createId(String value){
		long l = new Date().getTime();
		switch (value) {
		case "QUESTION":
			return "QUESTION"+l;
		default:
			break;
		}
		return null;
	}


}
