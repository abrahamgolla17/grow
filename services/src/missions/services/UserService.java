package missions.services;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import missions.dto.User;
import missions.dto.UserAddress;

public interface UserService {
	public User validateUser(String mobileNo, String password, boolean console);

	public boolean createUser(User user, JSONArray addresses,Set<UserAddress> addressList);
	
	public boolean createMultipleUsers(List<User> users);

	public boolean updatePinorPassword(User user, JSONObject pinOrPasswordData, boolean pin);
	
	public List<User> searchUser(String firstName, String lastName, String phNum, String city, String userid);

	public boolean resetToDefaultPassword(String userId);
	
	public User getUserById(String userId);

	public boolean updateUser(JSONObject userObject);

	public boolean saveImage(User user);
	
	public User getUserByName(String username);

	public boolean updateUser(User user);

}
