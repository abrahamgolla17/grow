package missions.services;

import java.util.List;
import java.util.Map;

import missions.dto.Questionnaire;

public interface CommonServices {
	public boolean createQuestionaire(Questionnaire questionnaire);
	public List<Questionnaire> getAllQuestions();
	public Map<String, Integer> getStatistics(long from , long till);
}
