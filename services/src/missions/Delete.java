package missions;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import missions.util.ProjectConfig;

public class Delete {

	public static void main(String[] args) throws Exception {

		/*
		 * Image will be uploaded to super admin profile
		 * When user is added, get the image from super admin path and copy to user path
		 * */

		String newUserId = "USER15351395178531";

		ProjectConfig config = new ProjectConfig();
		String IMAGES_BASE_PATH = config.getImagesPath();

		String image = "http://localhost:8081/jmmimages/USER1531450325101/profile/mobile.jpg";

		URI url = new URI(image);
		String path = url.getPath();

		String imageName = path.substring(path.lastIndexOf("/")).replace("/", "");

		String oldImagePath = IMAGES_BASE_PATH+File.separator+path.substring(path.lastIndexOf("USER")).replace("/", File.separator);
		Path source = Paths.get(oldImagePath);

		String newImagePath = IMAGES_BASE_PATH+File.separator+newUserId+File.separator+"profile"+File.separator+imageName;
		if(!new File(IMAGES_BASE_PATH+File.separator+newUserId+File.separator+"profile").exists())
			new File(IMAGES_BASE_PATH+File.separator+newUserId+File.separator+"profile").mkdirs();
		
		Path destination = Paths.get(newImagePath);

		try{
			Files.move(source, destination);
			String newUri = config.getImageIpandPort()+"/"+newUserId+"/profile/"+imageName;
			URI updatedUrl = new URI(newUri);
			System.out.println("Saving "+updatedUrl.getPath());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
