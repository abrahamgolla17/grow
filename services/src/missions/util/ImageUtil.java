package missions.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import missions.dto.User;

public class ImageUtil {

	private static Logger logger = Logger.getLogger(ImageUtil.class.getName());


	public static String saveImage(MultipartFile file, String imageType, User folderBasedonUser){
		logger.info(" file upload "+file);

		ProjectConfig config = new ProjectConfig();
		String IMAGES_BASE_PATH = config.getImagesPath();

		String imagePath = null;

		//Create folder for the image type if the image type folder doesn't exist
		if(folderBasedonUser != null && folderBasedonUser.getUserId() != null && !folderBasedonUser.getUserId().trim().isEmpty()){
			imagePath = IMAGES_BASE_PATH+File.separator+folderBasedonUser.getUserId()+File.separator+imageType;
		}else{
			imagePath = IMAGES_BASE_PATH+File.separator+imageType;
		}

		if(!new File(imagePath).exists())
			new File(imagePath).mkdirs();

		try {
			File imageFile = new File(imagePath+File.separator+file.getOriginalFilename());
			file.transferTo(imageFile);
			String imageUrl = buildUrl(imageType, file.getOriginalFilename(), folderBasedonUser.getUserId());
			return imageUrl;

		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<String> saveMultipleImage(MultipartFile[] files, String imageTypeBasedOnEvent, String folderBasedonUserId){
		logger.info(" file upload "+files.length);
		logger.info(" file type "+imageTypeBasedOnEvent);

		ProjectConfig config = new ProjectConfig();
		String IMAGES_BASE_PATH = config.getImagesPath();
		List<String> urls = new ArrayList<>();

		String imagePath = null;

		//Create folder for the image type if the image type folder doesn't exist
		if(folderBasedonUserId != null && !folderBasedonUserId.trim().isEmpty()){
			imagePath = IMAGES_BASE_PATH+File.separator+folderBasedonUserId+File.separator+imageTypeBasedOnEvent;
		}else{
			imagePath = IMAGES_BASE_PATH+File.separator+imageTypeBasedOnEvent;
		}


		if(!new File(imagePath).exists())
			new File(imagePath).mkdirs();

		if(files.length > 0){
			for(MultipartFile multipartFile : files){
				try {
					File file = new File(imagePath+File.separator+multipartFile.getOriginalFilename());
					multipartFile.transferTo(file);
					urls.add(buildUrl(imageTypeBasedOnEvent, multipartFile.getOriginalFilename(), folderBasedonUserId));
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		return urls;
	}
	private static String buildUrl(String path, String fileName, String userId){
		ProjectConfig config = new ProjectConfig();
		String url = config.getImageIpandPort()+"/"+userId+"/"+path+"/"+fileName;
		logger.info(url);
		return url;
	}

}
