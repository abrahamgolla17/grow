package missions.util;

import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.util.Base64Utils;

public class PasswordUtil {

	private static final int saltLen = 32;
	private static final int iterations = 1;
	private static final int desiredKeyLen = 256;

	/** 
	 * Computes a salted PBKDF2 hash of given plain text password suitable for storing in a database.
	 * Empty passwords are not supported. 
	 * */
	public static String getSaltedHash(String password) throws Exception {
		byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen);
		return Base64Utils.encodeToString(salt) + "$" + hash(password, salt);
	}

	private static String hash(String password, byte[] salt) throws Exception {

		if (password == null || password.length() == 0)
			throw new IllegalArgumentException("Empty passwords are not supported.");

		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey key = f.generateSecret(new PBEKeySpec(
				password.toCharArray(), salt, iterations, desiredKeyLen)
				);
		return Base64Utils.encodeToString(key.getEncoded());

	}

	/** Checks whether given plaintext password corresponds 
    to a stored salted hash of the password. */
	public static boolean check(String password, String stored) throws Exception{
		String[] saltAndPass = stored.split("\\$");
		if (saltAndPass.length != 2) {
			throw new IllegalStateException(
					"The stored password should have the form 'salt$hash'");
		}
		//byte[] hashOfInput = hash(password, Base64.decodeBase64(saltAndPass[0].getBytes()));

		String hashOfInput = hash(password, Base64Utils.decodeFromString(saltAndPass[0]));
		//System.out.println("Hash of input " + hashOfInput + " Stored hash " + saltAndPass[1]);
		return hashOfInput.equals(saltAndPass[1]);	
	}
	
	public static String decodeBase64(String value){
		
		byte[]  decodedBytes = Base64.getDecoder().decode(value.replaceAll("Basic ", ""));
		String decodedString = new String(decodedBytes);
		return decodedString;
	}
	
}
