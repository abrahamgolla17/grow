package missions.resources;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import missions.resources.util.Response;


public interface UserResource {
	
	public ResponseEntity<Response> login(String authorizationHeader, boolean console);
	public ResponseEntity<Response> registerUser(String authorization, String user, boolean console);
	public ResponseEntity<Response> editUser(String authorization, String user, boolean console, String userId);
	public ResponseEntity<Response> updatePasswordOrPin(String authorization,String passwordDetails, boolean pin);
	public ResponseEntity<Response> resetPasswordAndPin(String authorization,boolean console, String userId);
	public ResponseEntity<Response> addAllUsers(
			String authHeader,
			String type,MultipartFile file, HttpServletRequest request, boolean console);
	public ResponseEntity<Response> searchUser(String authorization, String firstName, String lastName, String phoneNumber, String city,String userid, boolean console);
	public ResponseEntity<Response> resetAdminPassword(String user,String passwordDetails);
	
}
