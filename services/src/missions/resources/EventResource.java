package missions.resources;

import org.springframework.http.ResponseEntity;

import missions.resources.util.Response;

public interface EventResource {
	public ResponseEntity<Response> createEvent(String authHeader, String eventDetails);
	
	public ResponseEntity<Response> getReportsBasedOnUserId(String authHeader, String userId,String reportType, boolean console);
	
	public ResponseEntity<Response> test();
	
	public ResponseEntity<Response> getAllReports(String authHeader, String reportType, boolean console);
	
	public ResponseEntity<Response> delete(String userID);
	
}
