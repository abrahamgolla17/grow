package missions.dto.constants;

public enum QuestionType {
	
	TEXT,
	BOOLEAN,
	MULTIPLE_CHOICE,
	DROPDOWN,
	COORDINATES,
	DATE
	
}
