package missions.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Table(name="jmm_questionaire_tbl",uniqueConstraints=@UniqueConstraint(columnNames = {"SERIAL_NO", "REPORT_TYPE"}))
public class Questionnaire implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="QUESTION_ID")
	private String questionId;
	
	@Column(name="QUESTION")
	private String question;
	
	@Column(name="QUESTION_TYPE")
	private String questionType;
	
	@Column(name="SECTION")
	private String questionSection;
	
	@Column(name="REPORT_TYPE")
	private String reportType;
	
	@Column(name="SERIAL_NO")
	private int serialNo;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PARENT_QUESTION")
	private Questionnaire parent;
	
	@OneToMany(mappedBy="question",fetch=FetchType.LAZY)
	private List<EventJoinQuestionnaire> eventsQuestionAnswers;

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getQuestionSection() {
		return questionSection;
	}

	public void setQuestionSection(String questionSection) {
		this.questionSection = questionSection;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	public Questionnaire getParent() {
		return parent;
	}

	public void setParent(Questionnaire parent) {
		this.parent = parent;
	}

	public List<EventJoinQuestionnaire> getEventsQuestionAnswers() {
		return eventsQuestionAnswers;
	}

	public void setEventsQuestionAnswers(List<EventJoinQuestionnaire> eventsQuestionAnswers) {
		this.eventsQuestionAnswers = eventsQuestionAnswers;
	}
}
